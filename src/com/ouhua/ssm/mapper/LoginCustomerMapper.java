package com.ouhua.ssm.mapper;

import com.ouhua.ssm.entity.LoginCustomer;

public interface LoginCustomerMapper {

	public LoginCustomer loginCustomer(LoginCustomer loginCustomer);
}
