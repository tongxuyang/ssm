package com.ouhua.ssm.mapper;

import com.ouhua.ssm.entity.ShopList;

public interface ShopInfoMapper {

	public void createShopInfo(ShopList shopList);
	public void updateShopInfo(ShopList shopList);
	public void deleteShopInfo(int id);
}
