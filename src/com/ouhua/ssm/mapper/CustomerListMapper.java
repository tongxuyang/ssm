package com.ouhua.ssm.mapper;

import com.ouhua.ssm.entity.CustomerList;




/**
 * CustomerList
 * @author Administrator
 *
 */
public interface CustomerListMapper {
	public CustomerList selectCustomerListById(int id);
	public void updateCustomerListById(CustomerList customerList);
	public void createCustomerDetailInfo(CustomerList customerList);
	public CustomerList selectCustomerInfoByUser(CustomerList customerList);
	
}
