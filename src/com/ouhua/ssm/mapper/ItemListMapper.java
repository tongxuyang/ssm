package com.ouhua.ssm.mapper;

import java.util.List;
import java.util.Map;

import com.ouhua.ssm.entity.ItemList;


/**
 * 
 * @author tongxuyang
 *
 */
public interface ItemListMapper {

	public List<ItemList> selectItemListsOrderByHot(Map map);
	public List<ItemList> getGoodsBasicInfoByPage(Map map);
	public ItemList getGoodsDetailInfo(int id);
	public void updateGoodsDetailInfo(ItemList itemList);
	public void createGoodsDetailInfo(ItemList itemList);
}
