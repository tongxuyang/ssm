package com.ouhua.ssm.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.ouhua.ssm.entity.ItemList;
import com.ouhua.ssm.mapper.ItemListMapper;


/**
 * 商品信息Service
 * @author tongxuyang
 *
 */
@Service
public class ItemListService {

	private ItemListMapper itemListMapper;

	public void setItemListMapper(ItemListMapper itemListMapper) {
		this.itemListMapper = itemListMapper;
	}
	/*分页拿取商品id信息，拿取规则为商品热度（hot）。*/
	public List<ItemList> getGoodsPoorInfoByPage(int page,int goodsNum,String thirdLevel){
		 Map<String,Object> map = new LinkedHashMap<String,Object>();
         map.put("page",(page-1)*goodsNum);
         map.put("goodsNum",goodsNum);
         map.put("thirdLevel", thirdLevel);
		return itemListMapper.selectItemListsOrderByHot(map);
		
	}
	/*分页拿取商品基础信息，拿取规则为商品热度（hot）。主要用于显示商品列表信息。 */
	public List<ItemList> getGoodsBasicInfoByPage(int page,int goodsNum,String thirdLevel){
		 Map<String,Object> map = new LinkedHashMap<String,Object>();
         map.put("page",(page-1)*goodsNum);
         map.put("goodsNum",goodsNum);
         map.put("thirdLevel", thirdLevel);
		return itemListMapper.getGoodsBasicInfoByPage(map);

	}
	/* 拿取商品所有信息。主要用于商品的详细信息展示页。 */
	public ItemList getGoodsDetailInfo(Integer id){
		return itemListMapper.getGoodsDetailInfo(id);
	}
	
	/*更新商品所有信息。主要用于商品的修改信息页。*/
	public void updateGoodsDetailInfo(ItemList itemList){
		itemListMapper.updateGoodsDetailInfo(itemList);
	}
	/*新建顾客所有信息。主要用于顾客的创建信息页。*/
	public void insertGoodsDetailInfo(ItemList itemList){
		itemListMapper.createGoodsDetailInfo(itemList);
	}
	
}
