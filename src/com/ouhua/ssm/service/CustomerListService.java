package com.ouhua.ssm.service;


import org.springframework.stereotype.Service;
import com.ouhua.ssm.entity.CustomerList;
import com.ouhua.ssm.mapper.CustomerListMapper;

/**
 * 顾客详细信息service
 * @author tongxuyang
 *
 */
@Service
public class CustomerListService {
    private CustomerListMapper customerListMapper;
	public void setCustomerListMapper(CustomerListMapper customerListMapper) {
	this.customerListMapper = customerListMapper;
}

	//通过查询客户详细信息
	public CustomerList getCustomerLists(int id) throws Exception{
		
		return customerListMapper.selectCustomerListById(id);
		
	}
	//用户登陆后查询所有信息
	public CustomerList getCustomerListByUser(CustomerList customerList){
		return customerListMapper.selectCustomerInfoByUser(customerList);

	}
	//修改客户详细信息
	public void updateCustomerLists(CustomerList customerList){
		customerListMapper.updateCustomerListById(customerList);
			
	}
	//创建客户详细信息
	public void createCustomerDetailInfo(CustomerList customerList){
		customerListMapper.createCustomerDetailInfo(customerList);
		
	}
	
}
