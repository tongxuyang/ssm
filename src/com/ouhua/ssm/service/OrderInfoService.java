package com.ouhua.ssm.service;


import java.util.List;

import org.springframework.stereotype.Service;

import com.ouhua.ssm.entity.OrderList;
import com.ouhua.ssm.mapper.OrderInfoMapper;

/**
 * 订单信息service
 * @author Administrator
 *
 */
@Service
public class OrderInfoService {
	
    private OrderInfoMapper orderInfoMapper;
	public void setOrderInfoMapper(OrderInfoMapper orderInfoMapper) {
	this.orderInfoMapper = orderInfoMapper;
}
	//创建订单
	public void createOrderInfoService(OrderList orderList){
		orderInfoMapper.createOrderInfo(orderList);

	}
	//更新订单
	public void updateOrderInfo(OrderList orderList){
		orderInfoMapper.updateOrderInfo(orderList);
		
	}
	//根据订单号查询订单详细信息
	public List<OrderList>  selectOrderInfoById(Integer id){
		
		return orderInfoMapper.selectOrderInfoById(id);
		
		
	}
	
}
