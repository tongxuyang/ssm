package com.ouhua.ssm.service;

import com.ouhua.ssm.entity.ShopList;
import com.ouhua.ssm.mapper.ShopInfoMapper;


/**
 * 商铺信息service
 * @author tongxuyang
 *
 */
public class ShopInfoService {

	private ShopInfoMapper shopInfoMapper;

	public void setShopInfoMapper(ShopInfoMapper shopInfoMapper) {
		this.shopInfoMapper = shopInfoMapper;
	}
	//创建商铺信息 
	public void createShopInfoService(ShopList shopList){
		shopInfoMapper.createShopInfo(shopList);
	}
	
	//更新商铺信息
	public void updateShopInfoService(ShopList shopList){
		shopInfoMapper.updateShopInfo(shopList);
	}
	//删除商铺信息
	public void deleteShopInfoService(Integer id){
		shopInfoMapper.deleteShopInfo(id);
	}
	
}
