package com.ouhua.ssm.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ouhua.ssm.entity.OrderList;
import com.ouhua.ssm.service.OrderInfoService;

/**
 * 订单信息Controller
 * @author tongxuyang
 *
 */
@Controller
@RequestMapping("order")
public class OrderListController {

	@Autowired
	private OrderInfoService orderInfoService;
	/*创建订单*/
	@RequestMapping("create")
	@ResponseBody
	public Map<String, Object> createOrderInfo(OrderList orderList){
		Map<String, Object> map = new HashMap<String, Object>();
		if (orderList!=null) {
			orderInfoService.createOrderInfoService(orderList);
			map.put("result", "1");
			return map;
		}
		map.put("result", "0");
		return map;
	}
	
	/*更新订单*/
	@RequestMapping("update")
	@ResponseBody
	public Map<String, Object> updateOrderInfo(OrderList orderList){
		Map<String, Object> map = new HashMap<String, Object>();
		if(orderList!=null){
			orderInfoService.updateOrderInfo(orderList);
			map.put("result", "1");
			return map;
		}
		map.put("result", "0");
		return map;
	}
	/*根据订单号查询订单详细信息*/
	@RequestMapping("update")
	@ResponseBody
	public Map<String, Object> selectOrderListById(Integer id){
		Map<String, Object> map = new HashMap<String, Object>();
		List<OrderList> orderList=orderInfoService.selectOrderInfoById(id);
		map.put("orderList", orderList);
		return map;
	}
	
}
