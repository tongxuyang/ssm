package com.ouhua.ssm.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ouhua.ssm.entity.ShopList;
import com.ouhua.ssm.service.ShopInfoService;
/**
 * 商铺信息controller
 * @author tongxuyang
 *
 */
@Controller
@RequestMapping("shop")
public class ShopListController {

	
	@Autowired
	private ShopInfoService shopInfoService;
	
	/*创建商铺信息 */
	@RequestMapping("create")
	@ResponseBody
	public Map<String, Object> createShopInfo(ShopList shopList){
		Map<String, Object> map = new HashMap<String, Object>();
		if (shopList!=null) {
			shopInfoService.createShopInfoService(shopList);
			map.put("result", "1");
			return map;
		}
		map.put("result", "0");
		return map;
	}
	
	/*更新商铺信息*/
	@RequestMapping("update")
	@ResponseBody
	public Map<String, Object> updateShopInfo(ShopList shopList){
	Map<String, Object> map = new HashMap<String, Object>();
	if (shopList!=null) {
		shopInfoService.updateShopInfoService(shopList);
		map.put("result", "1");
		return map;
	}
	map.put("result", "0");
	return map;
	}
	
	/*删除商铺信息*/
	@RequestMapping("delete")
	@ResponseBody
	public Map<String, Object> deleteShopInfo(Integer id){
		Map<String, Object> map = new HashMap<String, Object>();
		if (id!=null) {
			shopInfoService.deleteShopInfoService(id);
			map.put("result", "1");
			return map;
		}
		map.put("result", "0");
		return map;
		}
		
	}
	
	

