package com.ouhua.ssm.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ouhua.ssm.entity.CustomerList;
import com.ouhua.ssm.service.CustomerListService;

/**
 * 顾客详细信息Controller
 * 
 * @author Administrator
 *
 */
@Controller
@Scope("prototype")
@RequestMapping("customerList")
public class CustomerListController {
	@Autowired
	private CustomerListService customerListService;
	Map<String, Object> map = new LinkedHashMap<String, Object>();

	/* 查询客户详细信息 */
	@RequestMapping("get")
	@ResponseBody
	public Map<String, Object> customer(int id) throws Exception {
		CustomerList customerList = customerListService.getCustomerLists(id);
		map.put("customerList", customerList);
		return map;

	}

	/* 修改客户详细信息 */
	@RequestMapping("update")
	@ResponseBody
	public Map<String, Object> updatecustomer(CustomerList customerList) {
		if (customerList != null) {
			customerListService.updateCustomerLists(customerList);
			map.put("result", "1");
			return map;
		}
		map.put("result", "0");
		return map;

	}

	/* 创建客户详细信息 */
	@RequestMapping("create")
	@ResponseBody
	public Map<String, Object> createCustomerDetailInfo(
			CustomerList customerList) {

		if (customerList != null) {
			customerListService.createCustomerDetailInfo(customerList);
			map.put("result", "1");
			return map;
		}
		map.put("result", "0");
		return map;
	}

}
