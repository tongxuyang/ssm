package com.ouhua.ssm.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ouhua.ssm.entity.ItemList;
import com.ouhua.ssm.service.ItemListService;

/**
 * 商品详情Controller
 * 
 * @author tongxuyang
 *
 */
@Controller
@RequestMapping("itemList")
public class ItemListController {
	@Autowired
	private ItemListService itemListService;

	/* 分页拿取商品id信息，拿取规则为商品热度（hot）。 */
	@RequestMapping("poor")
	@ResponseBody
	public Map<String, Object> selectGoodsPoorInfoByPage(Integer page,
			Integer goodsNum, String thirdLevel) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<ItemList> list = itemListService.getGoodsPoorInfoByPage(page,
					goodsNum, thirdLevel);
			map.put("result", list);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return map;

	}

	/* 分页拿取商品基础信息，拿取规则为商品热度（hot）。主要用于显示商品列表信息。 */
	@RequestMapping("basic")
	@ResponseBody
	public Map<String, Object> selectGoodsBasicInfoByPage(Integer page,
			Integer goodsNum, String thirdLevel) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<ItemList> list = itemListService.getGoodsBasicInfoByPage(page,
					goodsNum, thirdLevel);
			map.put("result", list);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return map;
	}

	/* 拿取商品所有信息。主要用于商品的详细信息展示页。 */
	@RequestMapping("detail")
	@ResponseBody
	public Map<String, Object> selectGoodsDetailInfo(Integer id) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			ItemList itemList = itemListService.getGoodsDetailInfo(id);
			map.put("result", itemList);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return map;
	}

	/* 更新商品所有信息。主要用于商品的修改信息页。 */
	@RequestMapping("update")
	@ResponseBody
	public Map<String, Object> updateGoodsDetailInfo(ItemList itemList) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (itemList != null) {
			itemListService.updateGoodsDetailInfo(itemList);
			map.put("result", "1");
			return map;
		}
		map.put("result", "0");
		return map;
	}

	/* 新建顾客所有信息。主要用于顾客的创建信息页。 */
	@RequestMapping("create")
	@ResponseBody
	public Map<String, Object> createGoodsDetailInfo(ItemList itemList) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (itemList != null) {
			itemListService.insertGoodsDetailInfo(itemList);
			map.put("result", "1");
			return map;
		}

		map.put("result", "0");
		return map;

	}
}
