package com.ouhua.ssm.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ouhua.ssm.entity.CustomerList;
import com.ouhua.ssm.entity.LoginCustomer;
import com.ouhua.ssm.service.CustomerListService;
import com.ouhua.ssm.service.LoginCustomerService;

@Controller
@RequestMapping("login")
public class LoginCustomerController {

	
	@Autowired
	private LoginCustomerService loginCustomerService;
	@Autowired
	private CustomerListService customerListService;
	@RequestMapping("toindex")
	@ResponseBody
	public Map<String, Object> loginCustomer(String nameType,String name, String password){
		Map<String, Object> map=new HashMap<String, Object>();
		LoginCustomer loginCustomer=new LoginCustomer();
		loginCustomer.setName(name);
		loginCustomer.setPassword(password);
		loginCustomer.setNameType(nameType);
		LoginCustomer login=loginCustomerService.getLoginCustomer(loginCustomer);
		if (login!=null) {
			CustomerList customerList=new CustomerList();
			customerList.setUser(name);
			CustomerList customer=customerListService.getCustomerListByUser(customerList);
			map.put("success", customer);
			return map;
		}
		map.put("message", "用户名或密码输入错误");
		return map;

	}
}
