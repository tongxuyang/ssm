package ssm;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.ouhua.ssm.entity.ShopList;
import com.ouhua.ssm.service.ShopInfoService;

public class ShopInfoServiceTest {
	ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application.xml");
	ShopInfoService shopInfoService=(ShopInfoService) applicationContext.getBean("shopInfoService");
	ShopList shopList=new ShopList();
	@Test
	public void testCreateShopInfoService(){
		shopList.setFirstLevel("APPLE");
		shopList.setSecondLevel("orange");
		shopList.setDesp("delicies");
		shopList.setFirstLevel("food");
		shopList.setAttribute("good");
		shopList.setMaster("tony");
		for (int i = 0; i < 100; i++) {
			shopInfoService.createShopInfoService(shopList);
		}
		
	}
	@Test
	public void testUpdateShopInfoService(){
		shopList.setId(1);
		shopList.setLivingOrder("this");
		shopInfoService.updateShopInfoService(shopList);
		
	}
	@Test
	public void testDeleteShopInfoService(){
		shopInfoService.deleteShopInfoService(1);
	}
	

}
