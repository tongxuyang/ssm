package ssm;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.ouhua.ssm.entity.CustomerList;
import com.ouhua.ssm.service.CustomerListService;



public class CustomerListServiceTest {
	CustomerList customerList=new CustomerList();
    @Test
	public void testSelectCuestomerListService() throws Exception{
		
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application.xml");
		CustomerListService customerListService= (CustomerListService) applicationContext.getBean("customerListService");
		CustomerList customerList = (CustomerList) customerListService.getCustomerLists(10000);
		System.out.println(customerList.getUser());
		
	}
    //修改客户信息
    @Test
    public void testUpdateCuestomerListService(){
    	ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application.xml");
		CustomerListService customerListService= (CustomerListService) applicationContext.getBean("customerListService");
		customerList.setId(10000);
		customerList.setUser("fanbingbing");
		customerList.setFirstName("fan");
		customerList.setSecondName("bingbign");
		customerList.setEmail("405950997@qq.com");
		customerList.setLevel("1000");
		customerList.setTel("18829581192");
		customerList.setVip("yes");
		customerList.setState("normal");
		customerListService.updateCustomerLists(customerList);
    	System.out.println("修改成功");
    }
	//添加客户信息
    @Test
    public void testCreateCustomerListService(){
    	ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application.xml");
		CustomerListService customerListService= (CustomerListService) applicationContext.getBean("customerListService");
	
		customerList.setUser("wangpeixiao");
		customerList.setFirstName("wang");
		customerList.setSecondName("bingbign");
		customerList.setEmail("405950997@qq.com");
		customerList.setLevel("1000");
		customerList.setTel("18829581192");
		customerList.setVip("yes");
		customerList.setState("normal");
		
		customerListService.createCustomerDetailInfo(customerList);
		
    }
    
}
